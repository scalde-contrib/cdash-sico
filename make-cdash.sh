#!/bin/bash
usage()
{
   echo "$(basename $0) <hostname>"
   echo 
   echo "Create a CDashboard on <hostname>"
}

[ $# -eq 1 ] || { usage; exit 1; }
command -v oc || { echo "The oc command must be available"; exit 2; }

hostname=$1
(echo "${hostname}" | grep -q '\.') || hostname=${hostname}.apps.math.cnrs.fr

project=$(echo ${hostname} | cut -d. -f1)

echo "Project name: ${project}"
echo "Dashboard URL: https://${hostname}"

set -e
oc new-project ${project} --display-name="CDash" --description="CDashboard"
oc new-app --template=openshift/mysql-persistent -p MYSQL_USER=cdash -p MYSQL_DATABASE=cdash
pw=$(oc get secret mysql --template='{{index .data "database-password"}}' | base64 -d)
oc new-app https://gricad-gitlab.univ-grenoble-alpes.fr/scalde-contrib/cdash-openshift.git -e CDASH_ROOT_ADMIN_PASS=SECRET -e CDASH_CONFIG='$CDASH_DB_LOGIN = "cdash"; $CDASH_DB_HOST = "mysql"; $CDASH_DB_PASS = "'${pw}'"; $CDASH_ASSET_URL = "https://'${hostname}'"; $CDASH_BASE_URL = "https://'${hostname}'";'
oc create route edge --service siconos-dashboard  --insecure-policy=Redirect --hostname ${hostname}
#oc annotate --overwrite route/siconos-dashboard haproxy.router.openshift.io/ip_whitelist=""

echo
echo "All done. Now run"
echo
echo "    oc status"
echo
echo "and wait until deployment/cdash-paas is running."
echo
echo "    oc wait --for=condition=Ready deployment/cdash-paas --timeout=200s"
echo
echo "Finally point your browser to https://${hostname} to do the initial installation."
echo

