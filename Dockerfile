FROM kitware/cdash:latest

# Openshift runs under random user id
# Make home dir and apache area writable
#RUN chmod -R a+w /home/kitware/cdash
RUN ls -al /home
#RUN ls -al /home/kitware
RUN chmod -R a+w /etc/apache2

# Change port numbers to 8080 instead 80 since
# we don't have the privileges for the latter
RUN sed -i 's/^Listen [0-9][0-9]*/Listen 8080/g' /etc/apache2/ports.conf
RUN sed -i 's/^<VirtualHost \*:[0-9][0-9]*>/<VirtualHost \*:8080>/g' \
    /etc/apache2/sites-enabled/*.conf
EXPOSE 8080